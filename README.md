# This is project has moved to Github.

Please follow [this link](https://github.com/rebrec/hurry/) to get up to date information about Hurry.


# Hurry

The (cross platform) SysAdmin's tool to get repetitive things done Fast.

![Hurry_Main](https://raw.githubusercontent.com/rebrec/hurry/master/docs/hurry_vSphere_result_ping.png)

[More screenshots](https://github.com/rebrec/hurry/blob/master/docs/GUI_Overview.md)


# Download latest builds

- [Windows Release](https://github.com/rebrec/hurry/releases)
- Linux Release (Not available yet. Fill an issue if interested)

